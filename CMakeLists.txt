cmake_minimum_required(VERSION 3.14)
project(AvlSet)

set(CMAKE_CXX_STANDARD 20)
set(CXX_STANDARD_REQUIRED true)

include(FetchContent)

FetchContent_Declare(fmt
  GIT_REPOSITORY https://github.com/fmtlib/fmt.git
  GIT_TAG master
)
FetchContent_MakeAvailable(fmt)

include_directories(src)
add_subdirectory(src)
