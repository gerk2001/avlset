﻿#include <avlset.h>
#include <fmt/core.h>

int main()
{
    AvlSet<int> set;

    fmt::print("\n\tInsert 10, 8, 14, 7, 12, 22\n");
    set.insert(10);
    set.insert(8);
    set.insert(14);
    set.insert(7);
    set.insert(12);
    set.insert(22);
    set.print();

    fmt::print("\n\tInsert 15\n");
    set.insert(15);
    set.print();

    fmt::print("\n\tInsert 16\n");
    set.insert(16);
    set.print();

    fmt::print("\n\tInsert 17\n");
    set.insert(17);
    set.print();

    fmt::print("\n\tInsert 18\n");
    set.insert(18);
    set.print();

    fmt::print("\n\tInsert 19\n");
    set.insert(19);
    set.print();

    fmt::print("\n\tInsert 20\n");
    set.insert(20);
    set.print();

    fmt::print("\n\tErase 7\n");
    set.erase(7);
    set.print();

    fmt::print("\n\tErase 8\n");
    set.erase(8);
    set.print();

    fmt::print("\n\tErase 16\n");
    set.erase(16);
    set.print();

    fmt::print("\n\tErase 15\n");
    set.erase(15);
    set.print();

    fmt::print("\n\tErase 12\n");
    set.erase(12);
    set.print();
}
