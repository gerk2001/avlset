#ifndef AVLSET_H
#define AVLSET_H

#include <algorithm>
#include <fmt/core.h>

template <class Value>
class AvlSet {
    struct Node;

public:
    bool insert(const Value& value)
    {
        auto result = insertInternal(value);
        if (result)
            size_++;

        return result;
    }
    bool erase(const Value& value)
    {
        auto result = eraseInternal(value);
        if (result)
            size_--;

        return result;
    }
    void print()
    {
        print(root_, 1);
    }
    bool contains(const Value& value)
    {
        return find(value).first;
    }
    bool empty() { return size_ == 0; }
    size_t size() { return size_; }

private:
    struct Node {
        Value value;
        Node
            *right,
            *left,
            *ancstr;
        int height;
    };

    size_t size_ = 0;
    Node* root_ = nullptr;

    std::pair<Node*, Node*> find(const Value& value, Node* hint = nullptr)
    {
        Node* node = hint ? hint : root_;
        Node* node_ancstr = node ? node->ancstr : nullptr;

        while (node != nullptr) {
            if (node->value == value)
                break;
            else {
                node_ancstr = node;
                node = (node->value < value) ? node->right
                                             : node->left;
            }
        }

        return { node, node_ancstr };
    }
    int height(Node* node) { return node ? node->height : 0; }
    int balance(Node* node) { return node ? (height(node->left) - height(node->right)) : 0; }
    auto rightRotate(Node* node)
    {
        Node* left_node = node->left;
        Node* T2 = left_node->right;
        left_node->right = node;
        left_node->ancstr = node->ancstr;
        if (left_node->ancstr) {
            if (left_node->ancstr->left == node)
                left_node->ancstr->left = left_node;
            else
                left_node->ancstr->right = left_node;
        }
        node->ancstr = left_node;
        node->left = T2;
        if (T2)
            T2->ancstr = node;
        node->height = 1 + std::max(height(node->left), height(node->right));
        left_node->height = 1 + std::max(height(left_node->left), height(left_node->right));

        return left_node;
    }
    auto leftRotate(Node* node)
    {
        Node* right_node = node->right;
        Node* T2 = right_node->left;
        right_node->left = node;
        right_node->ancstr = node->ancstr;
        if (right_node->ancstr) {
            if (right_node->ancstr->left == node)
                right_node->ancstr->left = right_node;
            else
                right_node->ancstr->right = right_node;
        }

        node->ancstr = right_node;
        node->right = T2;
        if (T2)
            T2->ancstr = node;
        node->height = 1 + std::max(height(node->left), height(node->right));
        right_node->height = 1 + std::max(height(right_node->left), height(right_node->right));

        return right_node;
    }
    void print(Node* node, int lvl)
    {
        if (node) {
            print(node->left, lvl + 1);
            for (int i = 0; i <= lvl; i++)
                fmt::print("    ");
            fmt::print("{}\n", node->value);
            print(node->right, lvl + 1);
        }
    }
    bool insertInternal(const Value& value, Node* hint = nullptr)
    {
        if (empty()) {
            root_ = new Node { value, nullptr, nullptr, nullptr, 1 };
            return true;
        } else if (auto [required_node, ancstr] = find(value, hint); required_node == nullptr && ancstr != nullptr) {
            auto new_node = new Node { value, nullptr, nullptr, ancstr, 1 };
            if (ancstr->value < value)
                ancstr->right = new_node;
            else
                ancstr->left = new_node;

            Node* node = ancstr;
            Node* root = node;
            while (node != nullptr) {

                node->height = 1 + std::max(height(node->left), height(node->right));

                if (auto node_balance = balance(node); std::abs(node_balance) > 1) {
                    if (node_balance > 1 && value < node->left->value) {
                        node = rightRotate(node);
                    } else if (node_balance < -1 && value > node->right->value) {
                        node = leftRotate(node);
                    } else if (node_balance > 1 && value > node->left->value) {
                        leftRotate(node->left);
                        node = rightRotate(node);
                    } else if (node_balance < -1 && value < node->right->value) {
                        rightRotate(node->right);
                        node = leftRotate(node);
                    }
                }

                root = node;
                node = node->ancstr;
            }

            root_ = root;
            return true;
        }
        return false;
    }
    bool eraseInternal(const Value& value, Node* hint = nullptr)
    {
        if (auto [required_node, ancstr] = find(value, hint); required_node) {
            if (required_node->left && required_node->right)
                deleteCaseHaveTwoSubtrees(required_node);
            else
                deleteCaseHaveOneOrZeroSubtree(required_node, ancstr);

            if (ancstr) {
                Node* node = ancstr;
                Node* root = node;
                while (node != nullptr) {
                    node->height = 1 + std::max(height(node->left), height(node->right));

                    auto node_balance = balance(node);
                    auto left_node_balance = balance(node->left);
                    auto right_node_balance = balance(node->right);

                    if (node_balance > 1 && left_node_balance >= 0) {
                        node = rightRotate(node);
                    } else if (node_balance > 1 && left_node_balance < 0) {
                        leftRotate(node->left);
                        node = rightRotate(node);
                    } else if (node_balance < -1 && right_node_balance <= 0) {
                        node = leftRotate(node);
                    } else if (node_balance < -1 && right_node_balance > 0) {
                        rightRotate(node->right);
                        node = leftRotate(node);
                    }

                    root = node;
                    node = node->ancstr;
                }

                root_ = root;
            }

            return true;
        } else
            return false;
    }
    Node* rightmostNode(Node* from)
    {
        Node* rm_node(from);
        while (rm_node->right != nullptr)
            rm_node = rm_node->right;
        return rm_node;
    }
    void deleteCaseHaveTwoSubtrees(Node* node)
    {
        auto replacement_node = rightmostNode(node->left);
        node->value = replacement_node->value;
        eraseInternal(replacement_node->value, replacement_node);
    }
    Node* deleteCaseHaveOneOrZeroSubtree(Node* node, Node* ancstr)
    {
        Node* replace_node(nullptr);
        if (node->left) {
            node->left->ancstr = ancstr;
            replace_node = node->left;
        } else if (node->right) {
            node->right->ancstr = ancstr;
            replace_node = node->right;
        }
        if (ancstr) {
            if (ancstr->left == node)
                ancstr->left = replace_node;
            else
                ancstr->right = replace_node;
        }
        delete node;
        if (root_ == node)
            root_ = replace_node;

        return replace_node;
    }
};

#endif // AVLSET_H
