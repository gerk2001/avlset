# AvlSet

Based on one of my university practice AvlTree-set implementation.

### Description

It is a template container, so you can use it with any data types or classes/structs satisfied these requirements:
- Comparison operators are defined.

### Requirements
- CMake 3.14 and higher (to compile as it's distributed)
- C++17 and higher

## Usage

Easy to use: just ```include<avlset.h>``` and declare ```AvlSet<Value>```, where ```Value``` is your data type and enjoy advantages of BST-tree.

### Supported operations

This implementation supports the following operations:
- **insert(const Value& value)** tries to perform insertion and returns **_true_** if value is not in the container, otherwise returns **_false_**. 
- **erase(const Value& value)**  tries to perform deletion and returns **_true_** if it was succesfull, otherwise returns **_false_**.
- **contains(const Value& value)** returns **_true_** if there is an element with value equivalent to value in the container, otherwise returns **_false_**.
- **size()** returns the number of elements in the container.
- **empty()** returns if the container is empty.
- **print()** to print the container using fmt(third-part library dependency).
### Roadmap
- overload of ```&``` operator for intersection of two given containers.
- overload of ```|``` operator for union of two given containers.
- public ```erase``` and ```insert``` functions with support ```hint-iterator```.
- constructor that takes ```std::initializer_list``` as a argument.
- STL-like iterators for using range-based loops and STL-algorithms.
- constructor that takes as an argument another AvlSet with the same type
- constructor that takes ```begin``` and ```end``` iterators of any other STL-containers.
- public **find(const Value& value)** method which returns STL-iterator.

### Support
Issues and pull-requests are welcome.

### License
For open source projects, say how it is licensed.
